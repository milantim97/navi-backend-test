<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Block\Get;

use Magento\Framework\View\Element\Template;

class User extends Template
{

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    )
    {
        parent::__construct($context, $data);
    }

    /**
     * @return User
     */
    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
}
