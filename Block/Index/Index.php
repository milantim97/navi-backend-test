<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Block\Index;

use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\StoreManagerInterface;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\Configuration;
use Magento\Framework\View\Element\Template;

class Index extends Template
{

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Collection
     */
    private $countryCollection;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param Context $context
     * @param Configuration $configuration
     * @param StoreManagerInterface $storeManager
     * @param Collection $countryCollection
     * @param Logger $logger
     * @param array $data
     */
    public function __construct(
        Context $context,
        Configuration $configuration,
        StoreManagerInterface $storeManager,
        Collection $countryCollection,
        Logger $logger,
        array $data = []
    ) {
        $this->configuration = $configuration;
        $this->storeManager = $storeManager;
        $this->countryCollection = $countryCollection;
        $this->logger = $logger;
        parent::__construct($context, $data);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (NoSuchEntityException $e) {
            $storeId = 0;
            $this->logger->error('Could not fetch current store.', [$e->getMessage()]);
        }

        return $storeId;
    }

    /**
     * Get all countries that are allowed via the configuration
     *
     * @param int|string $storeId
     * @return array
     */
    public function getAllowedCountries($storeId = 0): array
    {
        $selectedCountries = explode(',', $this->configuration->getSpecificCountries($storeId));
        $countryData = $this->countryCollection->loadData()->toOptionArray(false);

        $options[] = ['value' => '', 'label' => __('Use Random Supported Nationality')];
        foreach ($countryData as $data) {
            if (in_array($data['value'], $selectedCountries)) {
                $options[] = $data;
            }
        }

        return $options;
    }
}
