<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Block\Index;

use Magento\Directory\Model\ResourceModel\Country\Collection as CountryCollection;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\BuildCollectionFilters;
use NaviPartner\BackendTest\Model\Config\Source\Countries;
use NaviPartner\BackendTest\Model\ResourceModel\User\CollectionFactory;
use NaviPartner\BackendTest\Model\ResourceModel\User\Collection;

class Grid extends Template
{

    /**
     * @var CountryCollection
     */
    private $countryCollection;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var BuildCollectionFilters
     */
    private $buildCollectionFilters;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param Context $context
     * @param CountryCollection $countryCollection
     * @param Logger $logger
     * @param CollectionFactory $collectionFactory
     * @param BuildCollectionFilters $buildCollectionFilters
     * @param array $data
     */
    public function __construct(
        Context $context,
        CountryCollection $countryCollection,
        Logger $logger,
        CollectionFactory $collectionFactory,
        BuildCollectionFilters $buildCollectionFilters,
        array $data = []
    ) {
        $this->countryCollection = $countryCollection;
        $this->logger = $logger;
        $this->collectionFactory = $collectionFactory;
        $this->buildCollectionFilters = $buildCollectionFilters;
        parent::__construct($context, $data);
    }

    /**
     * Prepare layout for the Grid
     *
     * @return Grid|Collection
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $params = $this->getRequest()->getParams();

        if ($data = $this->getGridData()) {
            $data->getItems();
            try {
                $pager = $this->getLayout()
                    ->createBlock(Pager::class, 'user.grid.pager')
                    ->setData('params', $params)
                    ->setAvailableLimit([10 => 10, 20 => 20, 30 => 30])->setShowPerPage(true)
                    ->setCollection($data);

                $this->setChild('pager', $pager);
            } catch (LocalizedException $e) {
                $this->logger->error('Could not create Pager for the Grid.', [$e->getMessage()]);
            }

            return $data->load();
        }

        return $this;
    }

    /**
     * Ged Data for the Grid
     *
     * @return Collection
     */
    public function getGridData(): Collection
    {
        $request = $this->getRequest();
        $page = $request->getParam('p') ? $request->getParam('p') : 1;
        $pageSize = $request->getParam('limit') ? $request->getParam('limit') : 10;

        /*** @var Collection */
        $userCollection = $this->collectionFactory->create();

        $userCollection = $this->buildCollectionFilters->applyFilters($request, $userCollection);

        $userCollection->setOrder('created_at', 'ASC');
        $userCollection->setPageSize($pageSize);
        $userCollection->setCurPage($page);

        return $userCollection;
    }

    /**
     * Get Pager elements
     *
     * @return mixed
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * Get form action URL
     *
     * @return string
     */
    public function getFormAction(): string
    {
        return '/navipartner/index/grid';
    }

    /**
     * Get request params
     *
     * @return array
     */
    public function getRequestParams(): array
    {
        return $this->getRequest()->getParams();
    }

    /**
     * Remove All Filters from the Grid
     *
     * @return string
     */
    public function removeAllFilters(): string
    {
        return $this->getUrl('navipartner/index/grid');
    }

    /**
     * Remove Download current Grid data as PDF
     *
     * @return string
     */
    public function downloadGridAsPdf(): string
    {
        $params = $this->buildCollectionFilters->getFilters($this->getRequest());

        return $this->getUrl('navipartner/get/pdf', $params);
    }

    /**
     * Get Nationalities for selection
     *
     * @return array
     */
    public function getNationalities()
    {
        $countryData = $this->countryCollection->loadData()->toOptionArray(false);
        $options = [];

        foreach ($countryData as $data) {
            if (in_array($data['value'], Countries::API_SUPPORTED_COUNTRIES)) {
                $options[] = $data;
            }
        }

        return $options;
    }
}
