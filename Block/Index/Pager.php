<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Block\Index;

use Magento\Framework\View\Element\Template;
use Magento\Theme\Block\Html\Pager as MagentoPager;

class Pager extends MagentoPager
{

    /**
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        array $data = []
    ) {
        parent::__construct($context, $data);
    }

    /**
     * Retrieve page URL
     *
     * @param string $page
     *
     * @return string
     */
    public function getPageUrl($page)
    {
        $params = $this->getData('params');

        $params[$this->getPageVarName()] = $page > 1 ? $page : null;

        return $this->getPagerUrl($params);
    }

    /**
     * Get first page url
     *
     * @return string
     */
    public function getFirstPageUrl()
    {
        return $this->getPageUrl(1);
    }

    /**
     * Retrieve previous page URL
     *
     * @return string
     */
    public function getPreviousPageUrl()
    {
        return $this->getPageUrl($this->getCollection()->getCurPage(-1));
    }

    /**
     * Retrieve next page URL
     *
     * @return string
     */
    public function getNextPageUrl()
    {
        return $this->getPageUrl($this->getCollection()->getCurPage(+1));
    }

    /**
     * Retrieve last page URL
     *
     * @return string
     */
    public function getLastPageUrl()
    {
        return $this->getPageUrl($this->getCollection()->getLastPageNumber());
    }

    /**
     * Get limit url
     *
     * @param int $limit
     *
     * @return string
     */
    public function getLimitUrl($limit)
    {
        return $this->getPagerUrl($this->getPageLimitParams($limit));
    }

    /**
     * Return page limit params
     *
     * @param int $limit
     * @return array
     */
    private function getPageLimitParams(int $limit): array
    {
        $data = [$this->getLimitVarName() => $limit];
        $params = $this->getData('params');
        if (isset($params['limit'])) {
            unset($params['limit']);
        }

        $currentPage = $this->getCurrentPage();
        $availableCount = (int)ceil($this->getTotalNum() / $limit);
        if ($currentPage !== 1 && $availableCount < $currentPage) {
            $data = array_merge($data, [$this->getPageVarName() => $availableCount === 1 ? null : $availableCount]);
        }

        return array_merge($data, $params);
    }
}
