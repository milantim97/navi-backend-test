define(['jquery', 'Magento_Customer/js/customer-data', 'mage/translate'], function ($, customerData, $t) {
    'use strict';

    $.widget('np.userdata', {
        options: {
            getDataUrl: ''
        },

        _init: function () {
            window.getDataUrl = this.options.getDataUrl;
            window.currentRequest = null;

            $("#get-user-data").click(function () {
                let selectedCountry = $("#select-country").val();

                if (window.currentRequest) {
                    return;
                }

                $('body').trigger('processStart');
                window.currentRequest = $.ajax({
                    context: '#user-data',
                    url: window.getDataUrl,
                    type: "POST",
                    data: {selected_country: selectedCountry},
                    success: function (data) {
                        $('#user-data').html(data.output);
                    },
                    error: function (e) {
                        customerData.set('messages', {
                            messages: [{
                                text: $t('An error occurred please try again latter.'),
                                type: 'error'
                            }]
                        });
                    }
                }).always(function () {
                    window.currentRequest = null;
                    $('body').trigger('processStop');
                });
            });
        },
    });

    return $.np.userdata;
});
