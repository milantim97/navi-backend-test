<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\ScopeInterface;

class Configuration
{
    /**
     * Section config path
     */
    const XML_NAVI_PARTNER_SECTION_PATH = 'backend_test';

    /**
     * Group config path
     */
    const XML_GENERAL_GROUP_PATH = '/general/';
    const XML_USER_API_GROUP_PATH = '/user_api/';
    const XML_WEATHER_API_GROUP_PATH = '/weather_api/';

    /**
     * Fields
     */
    const XML_DEBUG_MODE_PATH = 'allow_debugging';
    const XML_SPECIFIC_COUNTRIES_PATH = 'specific_countries';
    const XML_ENDPOINT_PATH = 'endpoint';
    const XML_API_KEY_PATH = 'api_key';
    const XML_UNITS_PATH = 'units';
    const XML_DAYS_BACK_PATH = 'days_back';
    const XML_API_TIMEOUT_PATH = 'timeout';

    /**
     * @var ScopeConfigInterface
     */
    private $config;

    /**
     * @param ScopeConfigInterface $config
     */
    public function __construct(ScopeConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * Check if log debugging is enabled
     *
     * @param int|string $storeId
     * @return bool
     */
    public function isDebugMode($storeId = 0): bool
    {
        return $this->config->isSetFlag(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_GENERAL_GROUP_PATH . self::XML_DEBUG_MODE_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get countries enabled for the API
     *
     * @param int|string $storeId
     * @return string
     */
    public function getSpecificCountries($storeId = 0): string
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_GENERAL_GROUP_PATH . self::XML_SPECIFIC_COUNTRIES_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get the endpoint for the User API
     *
     * @param int|string $storeId
     * @return string
     */
    public function getRandomUserApiEndpoint($storeId = 0): string
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_USER_API_GROUP_PATH . self::XML_ENDPOINT_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get the endpoint for the Weather API
     *
     * @param int|string $storeId
     * @return mixed
     */
    public function getWeatherApiEndpoint($storeId = 0)
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_WEATHER_API_GROUP_PATH . self::XML_ENDPOINT_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get Weather API key
     *
     * @param int|string $storeId
     * @return mixed
     */
    public function getWeatherApiKey($storeId = 0)
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_WEATHER_API_GROUP_PATH . self::XML_API_KEY_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get units of measure for the Weather API
     *
     * @param int|string $storeId
     * @return mixed
     */
    public function getWeatherSelectedUnit($storeId = 0)
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_WEATHER_API_GROUP_PATH . self::XML_UNITS_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get how old in days should the data in the Grid be shown by default
     *
     * @param int|string $storeId
     * @return mixed
     */
    public function getDefaultDaysBack($storeId = 0)
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_GENERAL_GROUP_PATH . self::XML_DAYS_BACK_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Get API connection timeout
     *
     * @param int|string $storeId
     * @return mixed
     */
    public function getApiTimeout($storeId = 0)
    {
        return $this->config->getValue(
            self::XML_NAVI_PARTNER_SECTION_PATH . self::XML_GENERAL_GROUP_PATH . self::XML_API_TIMEOUT_PATH,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
}
