<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model\RandomUser;

use Laminas\Http\Client as LaminasClient;
use Laminas\Http\Headers;
use Laminas\Http\Request;
use Laminas\Stdlib\Parameters;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\Configuration;
use NaviPartner\BackendTest\Service\Client;

class RandomUser extends Client
{

    /**
     * @var Json
     */
    private $json;

    /**
     * @param LaminasClient $client
     * @param Parameters $parameters
     * @param Headers $headers
     * @param Request $request
     * @param Json $json
     * @param Logger $logger
     * @param Configuration $configuration
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        LaminasClient $client,
        Parameters $parameters,
        Headers $headers,
        Request $request,
        Json $json,
        Logger $logger,
        Configuration $configuration,
        StoreManagerInterface $storeManager
    ) {
        $this->json = $json;
        parent::__construct($client, $parameters, $headers, $request, $logger, $configuration, $storeManager);
    }

    /**
     * Make a request towards RandomUser API
     *
     * @param string $selectedCountry
     * @return array
     */
    public function getUserData(string $selectedCountry): array
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (NoSuchEntityException $e) {
            $storeId = 0;
        }

        if (!$selectedCountry) {
            $supportedCountries = explode(',', $this->configuration->getSpecificCountries($storeId));
            $key = array_rand($supportedCountries);
            $selectedCountry = $supportedCountries[$key];
        }

        $endpoint = $this->configuration->getRandomUserApiEndpoint($storeId);
        $params = ['nat' => $selectedCountry];

        $response = $this->call($endpoint, Request::METHOD_GET, $params);
        $responseData = [];

        if ($response && $response->getStatusCode() === 200) {
            try {
                $responseData = $this->json->unserialize($response->getBody());
            } catch (\Exception $e) {
                $this->logger->error('Could not decode JSON value.', [$response->getBody()]);
            }
        }

        return $this->cleanResponse($responseData);
    }

    /**
     * @param array $response
     * @return array
     */
    private function cleanResponse(array $response)
    {
        if (!isset($response['results'])) {
            return [];
        }

        $responseData = current($response['results']);

        return [
            'name' => $responseData['name']['first'] . ' ' . $responseData['name']['last'],
            'street' => implode(' ', array_reverse($responseData['location']['street'])),
            'city' => $responseData['location']['city'],
            'state' => $responseData['location']['state'],
            'phone' => $responseData['phone'],
            'postcode' => $responseData['location']['postcode'],
            'email' => $responseData['email'],
            'dob' => date('d-m-Y', strtotime($responseData['dob']['date'])),
            'coordinates' => $responseData['location']['coordinates'],
            'nationality' => $responseData['location']['country']
        ];
    }
}
