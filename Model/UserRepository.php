<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use NaviPartner\BackendTest\Api\Data\UserInterface;
use NaviPartner\BackendTest\Api\Data\UserInterfaceFactory;
use NaviPartner\BackendTest\Api\Data\UserSearchResultsInterfaceFactory;
use NaviPartner\BackendTest\Api\UserRepositoryInterface;
use NaviPartner\BackendTest\Model\ResourceModel\User as ResourceUser;
use NaviPartner\BackendTest\Model\ResourceModel\User\CollectionFactory as UserCollectionFactory;

class UserRepository implements UserRepositoryInterface
{

    /**
     * @var UserInterfaceFactory
     */
    protected $userFactory;

    /**
     * @var UserCollectionFactory
     */
    protected $userCollectionFactory;

    /**
     * @var User
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceUser
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;


    /**
     * @param ResourceUser $resource
     * @param UserInterfaceFactory $userFactory
     * @param UserCollectionFactory $userCollectionFactory
     * @param UserSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceUser $resource,
        UserInterfaceFactory $userFactory,
        UserCollectionFactory $userCollectionFactory,
        UserSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->userFactory = $userFactory;
        $this->userCollectionFactory = $userCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(UserInterface $user)
    {
        try {
            $this->resource->save($user);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the User: %1',
                $exception->getMessage()
            ));
        }
        return $user;
    }

    /**
     * @inheritDoc
     */
    public function get($entityId)
    {
        $user = $this->userFactory->create();
        $this->resource->load($user, $entityId);
        if (!$user->getId()) {
            throw new NoSuchEntityException(__('User with id "%1" does not exist.', $entityId));
        }
        return $user;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->userCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(UserInterface $user)
    {
        try {
            $userModel = $this->userFactory->create();
            $this->resource->load($userModel, $user->getEntityId());
            $this->resource->delete($userModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the user: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($entityId)
    {
        return $this->delete($this->get($entityId));
    }
}

