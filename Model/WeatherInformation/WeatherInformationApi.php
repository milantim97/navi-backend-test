<?php

namespace NaviPartner\BackendTest\Model\WeatherInformation;

use Laminas\Http\Client as LaminasClient;
use Laminas\Http\Headers;
use Laminas\Http\Request;
use Laminas\Stdlib\Parameters;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Store\Model\StoreManagerInterface;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\Configuration;
use NaviPartner\BackendTest\Service\Client;

class WeatherInformationApi extends Client
{

    /**
     * @var Json
     */
    private $json;

    /**
     * @param LaminasClient $client
     * @param Parameters $parameters
     * @param Headers $headers
     * @param Request $request
     * @param Json $json
     * @param Logger $logger
     * @param Configuration $configuration
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        LaminasClient $client,
        Parameters $parameters,
        Headers $headers,
        Request $request,
        Json $json,
        Logger $logger,
        Configuration $configuration,
        StoreManagerInterface $storeManager
    ) {
        $this->json = $json;
        parent::__construct($client, $parameters, $headers, $request, $logger, $configuration, $storeManager);
    }

    /**
     * Make a request towards Weather API
     *
     * @param string $lat
     * @param string $long
     * @return array
     */
    public function getWeatherInformation(string $lat, string $long): array
    {
        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (NoSuchEntityException $e) {
            $storeId = 0;
        }

        $endpoint = $this->configuration->getWeatherApiEndpoint($storeId);
        $apiKey = $this->configuration->getWeatherApiKey($storeId);
        $units = $this->configuration->getWeatherSelectedUnit($storeId);

        $params = [
            'lat' => $lat,
            'lon' => $long,
            'appid' => $apiKey,
            'units' => $units
        ];

        $response = $this->call($endpoint, Request::METHOD_GET, $params);
        $responseData = [];

        if ($response && $response->getStatusCode() === 200) {
            try {
                $responseData = $this->json->unserialize($response->getBody());
            } catch (\Exception $e) {
                $this->logger->error('Could not decode JSON value.', [$response->getBody()]);
            }
        }

        return $this->cleanResponse($responseData);
    }

    /**
     * Clean the API response and retrieve relevant data
     *
     * @param array $responseData
     * @return array
     */
    private function cleanResponse(array $responseData)
    {
        if (!$responseData) {
            return [];
        }

        $weatherCombination = '';
        foreach ($responseData['weather'] as $weather) {
            if ($weatherCombination) {
                $weatherCombination .= '/' . $weather['main'];
                continue;
            }

            $weatherCombination = $weather['main'];
        }

        return [
            'weather' => $weatherCombination,
            'temp' => $responseData['main']['temp'],
            'feels_like' => $responseData['main']['feels_like'],
            'pressure' => $responseData['main']['pressure'],
            'humidity' => $responseData['main']['humidity'],
        ];
    }
}
