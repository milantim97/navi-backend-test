<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Units implements ArrayInterface
{
    public const API_SUPPORTED_UNITS = [
        'standard' => 'Standard',
        'metric' => 'Metric',
        'imperial' => 'Imperial'
    ];

    /**
     * Return options array
     *
     * @param boolean $isMultiselect
     * @param string|array $foregroundCountries
     * @return array
     */
    public function toOptionArray($isMultiselect = false, $foregroundCountries = '')
    {
        $options = [];
        $count = 0;
        foreach (self::API_SUPPORTED_UNITS as $value => $label) {
            $options[$count]['value'] =  $value;
            $options[$count]['label'] =  $label;
            $count++;
        }

        return $options;
    }
}
