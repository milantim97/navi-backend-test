<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model\Config\Source;

use Magento\Directory\Model\ResourceModel\Country\Collection;
use Magento\Framework\Option\ArrayInterface;

class Countries implements ArrayInterface
{
    public const API_SUPPORTED_COUNTRIES = [
        'AU',
        'BR',
        'CA',
        'CH',
        'DE',
        'DK',
        'ES',
        'FI',
        'FR',
        'GB',
        'IE',
        'IR',
        'NO',
        'NL',
        'NZ',
        'TR',
        'US'
    ];

    /**
     * @var Collection
     */
    protected $countryCollection;

    /**
     * @var array
     */
    protected $options;

    /**
     * @param Collection $countryCollection
     */
    public function __construct(Collection $countryCollection)
    {
        $this->countryCollection = $countryCollection;
    }

    /**
     * Return options array
     *
     * @param boolean $isMultiselect
     * @param string|array $foregroundCountries
     * @return array
     */
    public function toOptionArray($isMultiselect = false, $foregroundCountries = '')
    {
        if (!$this->options) {
            $this->options = $this->countryCollection->loadData()->setForegroundCountries(
                $foregroundCountries
            )->toOptionArray(
                false
            );
        }

        $options = [];
        foreach ($this->options as $option) {
            if (in_array($option['value'], self::API_SUPPORTED_COUNTRIES)) {
                $options[] = $option;
            }
        }

        return $options;
    }
}
