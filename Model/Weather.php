<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model;

use Magento\Framework\Model\AbstractModel;
use NaviPartner\BackendTest\Api\Data\WeatherInterface;

class Weather extends AbstractModel implements WeatherInterface
{

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(ResourceModel\Weather::class);
    }

    /**
     * @inheritDoc
     */
    public function getEntityId()
    {
        return $this->getData(self::ENTITY_ID);
    }

    /**
     * @inheritDoc
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * @inheritDoc
     */
    public function getWeather()
    {
        return $this->getData(self::WEATHER);
    }

    /**
     * @inheritDoc
     */
    public function setWeather($weather)
    {
        return $this->setData(self::WEATHER, $weather);
    }

    /**
     * @inheritDoc
     */
    public function getTemp()
    {
        return $this->getData(self::TEMP);
    }

    /**
     * @inheritDoc
     */
    public function setTemp($temp)
    {
        return $this->setData(self::TEMP, $temp);
    }

    /**
     * @inheritDoc
     */
    public function getFeelsLike()
    {
        return $this->getData(self::FEELS_LIKE);
    }

    /**
     * @inheritDoc
     */
    public function setFeelsLike($feelsLike)
    {
        return $this->setData(self::FEELS_LIKE, $feelsLike);
    }

    /**
     * @inheritDoc
     */
    public function getPressure()
    {
        return $this->getData(self::PRESSURE);
    }

    /**
     * @inheritDoc
     */
    public function setPressure($pressure)
    {
        return $this->setData(self::PRESSURE, $pressure);
    }

    /**
     * @inheritDoc
     */
    public function getHumidity()
    {
        return $this->getData(self::HUMIDITY);
    }

    /**
     * @inheritDoc
     */
    public function setHumidity($humidity)
    {
        return $this->setData(self::HUMIDITY, $humidity);
    }

    /**
     * @inheritDoc
     */
    public function getUserId()
    {
        return $this->getData(self::USER_ID);
    }

    /**
     * @inheritDoc
     */
    public function setUserId($userId)
    {
        return $this->setData(self::USER_ID, $userId);
    }

    /**
     * @inheritDoc
     */
    public function getCheckedAt()
    {
        return $this->getData(self::CHECKED_AT);
    }

    /**
     * @inheritDoc
     */
    public function setCheckedAt($checkedAt)
    {
        return $this->setData(self::CHECKED_AT, $checkedAt);
    }
}
