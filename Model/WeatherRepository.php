<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model;

use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use NaviPartner\BackendTest\Api\Data\WeatherInterface;
use NaviPartner\BackendTest\Api\Data\WeatherInterfaceFactory;
use NaviPartner\BackendTest\Api\Data\WeatherSearchResultsInterfaceFactory;
use NaviPartner\BackendTest\Api\WeatherRepositoryInterface;
use NaviPartner\BackendTest\Model\ResourceModel\Weather as ResourceWeather;
use NaviPartner\BackendTest\Model\ResourceModel\Weather\CollectionFactory as WeatherCollectionFactory;

class WeatherRepository implements WeatherRepositoryInterface
{

    /**
     * @var Weather
     */
    protected $searchResultsFactory;

    /**
     * @var ResourceWeather
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    protected $collectionProcessor;

    /**
     * @var WeatherInterfaceFactory
     */
    protected $weatherFactory;

    /**
     * @var WeatherCollectionFactory
     */
    protected $weatherCollectionFactory;

    /**
     * @param ResourceWeather $resource
     * @param WeatherInterfaceFactory $weatherFactory
     * @param WeatherCollectionFactory $weatherCollectionFactory
     * @param WeatherSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        ResourceWeather $resource,
        WeatherInterfaceFactory $weatherFactory,
        WeatherCollectionFactory $weatherCollectionFactory,
        WeatherSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource = $resource;
        $this->weatherFactory = $weatherFactory;
        $this->weatherCollectionFactory = $weatherCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(WeatherInterface $weather)
    {
        try {
            $this->resource->save($weather);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the weather: %1',
                $exception->getMessage()
            ));
        }
        return $weather;
    }

    /**
     * @inheritDoc
     */
    public function get($entityId)
    {
        $weather = $this->weatherFactory->create();
        $this->resource->load($weather, $entityId);
        if (!$weather->getId()) {
            throw new NoSuchEntityException(__('Weather with id "%1" does not exist.', $entityId));
        }
        return $weather;
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $collection = $this->weatherCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(WeatherInterface $weather)
    {
        try {
            $weatherModel = $this->weatherFactory->create();
            $this->resource->load($weatherModel, $weather->getEntityId());
            $this->resource->delete($weatherModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Weather: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($entityId)
    {
        return $this->delete($this->get($entityId));
    }
}
