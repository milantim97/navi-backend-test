<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Model\ResourceModel\Weather;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{

    /**
     * @inheritDoc
     */
    protected $_idFieldName = 'entity_id';

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            \NaviPartner\BackendTest\Model\Weather::class,
            \NaviPartner\BackendTest\Model\ResourceModel\Weather::class
        );
    }
}

