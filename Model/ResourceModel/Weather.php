<?php
declare(strict_types=1);

namespace NaviPartner\BackendTest\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Weather extends AbstractDb
{

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('navipartner_weather', 'entity_id');
    }
}
