<?php

namespace NaviPartner\BackendTest\Model;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\TimezoneInterface;
use Magento\Store\Model\StoreManagerInterface;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\ResourceModel\User\Collection;
use NaviPartner\BackendTest\Model\ResourceModel\User\CollectionFactory;

class BuildCollectionFilters
{

    private const FILTER_PARAMS = [
        'name',
        'nationality',
        'to',
        'from',
        'city',
        'street',
        'state'
    ];
    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var Configuration
     */
    private $configuration;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var TimezoneInterface
     */
    private $date;

    /**
     * @param Logger $logger
     * @param Configuration $configuration
     * @param StoreManagerInterface $storeManager
     * @param CollectionFactory $collectionFactory
     * @param TimezoneInterface $date
     */
    public function __construct(
        Logger $logger,
        Configuration $configuration,
        StoreManagerInterface $storeManager,
        CollectionFactory $collectionFactory,
        TimezoneInterface $date
    ) {
        $this->logger = $logger;
        $this->configuration = $configuration;
        $this->storeManager = $storeManager;
        $this->collectionFactory = $collectionFactory;
        $this->date = $date;
    }

    /**
     * Apply any submitted filters
     *
     * @param RequestInterface $request
     * @param false|Collection $userCollection
     * @return Collection
     */
    public function applyFilters(RequestInterface $request, $userCollection = false): Collection
    {
        if (!$userCollection) {
            /*** @var Collection */
            $userCollection = $this->collectionFactory->create();
        }

        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (NoSuchEntityException $e) {
            $this->logger->error('Could not fetch current store.', [$e->getMessage()]);
            $storeId = 0;
        }

        $currentTime = $this->date->date()->format('Y-m-d H:i:s');
        $daysBack = '-' . $this->configuration->getDefaultDaysBack($storeId) . ' days';
        $history = strtotime($daysBack, strtotime($currentTime));

        $userCollection = $this->joinWeatherAndUser($userCollection);

        if (!$filters = $this->getFilters($request)) {
            return $userCollection->addFieldToFilter('created_at', [
                'gteq' => date('Y-m-d H-i-s', $history)
            ]);
        }

        foreach ($filters as $filter => $value) {
            switch ($filter) {
                case 'to':
                    if ($value) {
                        $to = strtotime($filters['to']);
                        $userCollection->addFieldToFilter('created_at', [
                            'lteq' => date('Y-m-d H-i-s', $to)
                        ]);
                    }
                    break;
                case 'from':
                    if ($value) {
                        $from = strtotime($filters['from']);

                        $userCollection->addFieldToFilter('created_at', [
                            'gteq' => date('Y-m-d H-i-s', $from)
                        ]);
                    }
                    break;
                default:
                    if ($value) {
                        $userCollection->addFieldToFilter($filter, ['like' => '%' . $value . '%']);
                    }
                    break;
            }
        }

        return $userCollection;
    }

    /**
     * Get only the supported filters
     *
     * @param $request
     * @return array
     */
    public function getFilters($request): array
    {
        $params = $request->getParams();
        $filters = [];

        foreach ($params as $param => $value) {
            if (in_array($param, self::FILTER_PARAMS)) {
                $filters[$param] = $value;
            }
        }

        return $filters;
    }

    /**
     * Join Weather and User tables
     *
     * @param $userCollection
     * @return mixed
     */
    public function joinWeatherAndUser($userCollection)
    {
        $userCollection->getSelect()->join(
            ['weather' => $userCollection->getTable('navipartner_weather')],
            'weather.user_id = main_table.entity_id',
            [
                'weather_id' => 'weather.entity_id',
                'weather' => 'weather.weather',
                'temperature' => 'weather.temp',
                'feels_like' => 'weather.feels_like',
                'pressure' => 'weather.pressure',
                'humidity' => 'weather.humidity',
                'created_at' => 'main_table.created_at',
                'checked_at' => 'weather.checked_at',
                'name' => 'main_table.name'
            ]
        );

        return $userCollection;
    }
}
