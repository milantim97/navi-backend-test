<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Service;

use Laminas\Http\Client as LaminasClient;
use Laminas\Http\Headers;
use Laminas\Http\Request;
use Laminas\Http\Response;
use Laminas\Stdlib\Parameters;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\Configuration;

abstract class Client
{
    private const HEADERS = [
        'Accept' => 'application/json',
        'Content-Type' => 'application/json'
    ];

    /**
     * @var LaminasClient
     */
    protected $client;

    /**
     * @var Parameters
     */
    protected $parameters;

    /**
     * @var Headers
     */
    protected $headers;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Configuration
     */
    protected $configuration;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @param LaminasClient $client
     * @param Parameters $parameters
     * @param Headers $headers
     * @param Request $request
     * @param Logger $logger
     * @param Configuration $configuration
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        LaminasClient $client,
        Parameters $parameters,
        Headers $headers,
        Request $request,
        Logger $logger,
        Configuration $configuration,
        StoreManagerInterface $storeManager
    ) {
        $this->client = $client;
        $this->parameters = $parameters;
        $this->headers = $headers;
        $this->request = $request;
        $this->logger = $logger;
        $this->configuration = $configuration;
        $this->storeManager = $storeManager;
    }

    /**
     * Client for the REST API requests
     *
     * @param string $endpoint
     * @param string $method
     * @param array $headers
     * @param array $parameters
     * @return Response|false
     */
    protected function call(string $endpoint, string $method, array $parameters = [], array $headers = []): Response
    {
        $this->headers->addHeaders(array_merge(self::HEADERS, $headers));
        $this->parameters->fromArray($parameters);

        try {
            $storeId = $this->storeManager->getStore()->getId();
        } catch (NoSuchEntityException $e) {
            $storeId = 0;
        }

        $debugMode = $this->configuration->isDebugMode($storeId);
        $timeout = $this->configuration->getApiTimeout($storeId);

        $options = [
            'maxredirects' => 0,
            'timeout' => $timeout
        ];

        $this->request->setHeaders($this->headers);
        $this->request->setUri($endpoint);
        $this->request->setMethod($method);
        $this->request->setQuery($this->parameters);
        $this->client->setOptions($options);

        try {
            $response = $this->client->send($this->request);
        } catch (\Exception $e) {
            $this->logger->error('Could not make the call towards ' . $endpoint, [
                'Message' => $e->getMessage(),
                'Request' => [
                    'Endpoint' => $this->request->getUriString(),
                    'Params' => $parameters
                ],
            ]);

            return false;
        }

        if ($debugMode) {
            $logData = $method . 'Request:' . $this->request->getUriString() . ' Response Status: ' . $response->getStatusCode();
            $context = [
                'Request' => [
                    'Endpoint' => $this->request->getUriString(),
                    'Params' => $parameters
                ],
                'Response' => $response->getBody()
            ];

            $this->logger->debug($logData, $context);
        }

        return $response;
    }
}
