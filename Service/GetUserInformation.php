<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Service;

use Magento\Framework\App\RequestInterface;
use NaviPartner\BackendTest\Api\GetUserInformationInterface;
use NaviPartner\BackendTest\Model\BuildCollectionFilters;

class GetUserInformation implements GetUserInformationInterface
{
    /**
     * @var BuildCollectionFilters
     */
    private $buildCollectionFilters;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @param BuildCollectionFilters $buildCollectionFilters
     * @param RequestInterface $request
     */
    public function __construct(
        BuildCollectionFilters $buildCollectionFilters,
        RequestInterface $request
    ) {
        $this->buildCollectionFilters = $buildCollectionFilters;
        $this->request = $request;
    }

    /**
     * @inheritDoc
     */
    public function execute(): array
    {
        $userCollection = $this->buildCollectionFilters->applyFilters($this->request);

        return $userCollection->toArray()['items'];
    }
}
