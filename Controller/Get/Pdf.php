<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Controller\Get;

use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Filesystem;
use Magento\Framework\Message\ManagerInterface;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\BuildCollectionFilters;
use Zend_Pdf;
use Zend_Pdf_Font;
use Zend_Pdf_Page;
use Zend_Pdf_Resource_Font;
use Zend_Pdf_Style;

class Pdf implements HttpGetActionInterface
{
    private const FIRST_PAGE_ITEM_COUNT = 27;
    private const SECOND_PAGE_ONWARD_ITEM_COUNT = 30;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var BuildCollectionFilters
     */
    private $buildCollectionFilters;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var int
     */
    private $x;

    /**
     * @var int
     */
    private $y;

    /**
     * @var Zend_Pdf
     */
    private $pdf;

    /**
     * @var Zend_Pdf_Style
     */
    private $style;

    /**
     * @var Zend_Pdf_Resource_Font
     */
    private $font;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var ResultFactory
     */
    private $resultFactory;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * @param Logger $logger
     * @param FileFactory $fileFactory
     * @param BuildCollectionFilters $buildCollectionFilters
     * @param RequestInterface $request
     * @param Zend_Pdf $pdf
     * @param Zend_Pdf_Style $style
     * @param Filesystem $filesystem
     * @param ResultFactory $resultFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Logger $logger,
        FileFactory $fileFactory,
        BuildCollectionFilters $buildCollectionFilters,
        RequestInterface $request,
        Zend_Pdf $pdf,
        Zend_Pdf_Style $style,
        Filesystem $filesystem,
        ResultFactory $resultFactory,
        ManagerInterface $messageManager
    ) {
        $this->fileFactory = $fileFactory;
        $this->logger = $logger;
        $this->buildCollectionFilters = $buildCollectionFilters;
        $this->request = $request;
        $this->pdf = $pdf;
        $this->style = $style;
        $this->filesystem = $filesystem;
        $this->resultFactory = $resultFactory;
        $this->messageManager = $messageManager;
    }

    /**
     *  Generate User data PDF
     *
     * @return ResultInterface|void
     */
    public function execute()
    {
        $userCollection = $this->buildCollectionFilters->applyFilters($this->request);

        $pdf = $this->pdf;
        $style = $this->style;

        $pdf->pages[] = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
        $pageNum = 0;
        $page = $pdf->pages[$pageNum];

        try {
            $rootDirectory = $this->filesystem->getDirectoryRead(DirectoryList::ROOT);
            $this->font = Zend_Pdf_Font::fontWithPath($rootDirectory
                ->getAbsolutePath('lib/internal/GnuFreeFont/FreeSerif.ttf'));
        } catch (\Zend_Pdf_Exception $e) {
            $this->logger->error('Could not acquire the Magento Font.', [$e->getMessage()]);
            return $this->handleException();
        }

        $style->setFont($this->font, 15);
        $page->setStyle($style);

        $this->x = 10;
        $this->y = 850 - 100;
        $legend = $this->y + 10;

        $this->drawLegend($page, $style);

        $style->setFont($this->font, 10);
        $page->setStyle($style);
        $this->y -= 10;
        $this->drawTableHeader($page);
        $page->drawRectangle(10, $legend, $page->getWidth() - 10, $this->y - 10,
            Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $style->setFont($this->font, 8);
        $page->setStyle($style);

        $count = 0;
        foreach ($userCollection as $user) {
            if (($count >= self::FIRST_PAGE_ITEM_COUNT && $pageNum === 0)
                || ($count >= self::SECOND_PAGE_ONWARD_ITEM_COUNT && $pageNum > 0)) {
                $count = 0;
                $pageNum++;

                $page = $this->switchPage($page, $pdf, $pageNum, $style, $legend);
            }

            $this->y -= 25;
            $count++;

            $this->drawTableContent($page, $user);
        }

        if ($pageNum > 0) {
            $legend = 790;
        }

        $this->y -= 20;
        $page->drawRectangle(10, $legend, $page->getWidth() - 10, $this->y, Zend_Pdf_Page::SHAPE_DRAW_STROKE);

        $fileName = 'User Information.pdf';

        try {
            $this->fileFactory->create(
                $fileName,
                $pdf->render(),
                DirectoryList::VAR_DIR,
                'application/pdf'
            );
        } catch (\Exception $e) {
            $this->logger->error('Could not create the PDF.', [$e->getMessage()]);

            return $this->handleException();
        }
    }

    /**
     * Switch the page once the limit of the items has been reached
     *
     * @param $page
     * @param $pdf
     * @param $pageNum
     * @param $style
     * @param $legend
     * @return mixed
     */
    public function switchPage($page, $pdf, $pageNum, $style, $legend)
    {
        $this->y -= 30;

        if ($pageNum > 0) {
            $page->drawRectangle(10, 820, $page->getWidth() - 10, $this->y,
                Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        } else {
            $page->drawRectangle(10, 810, $page->getWidth() - 10, $this->y, Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        }

        $pdf->pages[] = $pdf->newPage(Zend_Pdf_Page::SIZE_A4);
        $page = $pdf->pages[$pageNum];
        $this->y = 850 - 40;

        $page->setStyle($style);
        $page->drawRectangle(10, $this->y + 10, $page->getWidth() - 10, $this->y - 20,
            Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $style->setFont($this->font, 10);
        $page->setStyle($style);
        $this->y -= 10;

        $this->drawTableHeader($page);

        $style->setFont($this->font, 8);
        $page->setStyle($style);

        return $page;
    }

    /**
     * Draw Legend on top of the first page of the PDF
     *
     * @param $page
     * @param $style
     * @return void
     */
    private function drawLegend($page, $style)
    {
        $style->setFont($this->font, 16);
        $page->setStyle($style);
        $page->drawRectangle(10, $this->y + 10, $page->getWidth() - 10, $this->y + 70,
            Zend_Pdf_Page::SHAPE_DRAW_STROKE);
        $style->setFont($this->font, 15);
        $page->setStyle($style);
        $page->drawText(__("User Information"), $this->x + 5, $this->y + 50, 'UTF-8');
        $style->setFont($this->font, 11);
        $page->setStyle($style);
        $page->drawText(__("Temperature as Temp"), $this->x + 5, $this->y + 33, 'UTF-8');
        $page->drawText(__("Feels Like as LF"), $this->x + 5, $this->y + 16, 'UTF-8');
    }

    /**
     * Draw header of the table in the PDF
     *
     * @param $page
     * @return void
     */
    private function drawTableHeader($page)
    {
        $page->drawText(__("Full Name"), $this->x + 10, $this->y, 'UTF-8');
        $page->drawText(__("Email"), $this->x + 110, $this->y, 'UTF-8');
        $page->drawText(__("Address, City, Postcode, State"), $this->x + 240, $this->y, 'UTF-8');
        $page->drawText(__("Nationality"), $this->x + 380, $this->y, 'UTF-8');
        $page->drawText(__("Weather"), $this->x + 450, $this->y, 'UTF-8');
        $page->drawText(__("Temp"), $this->x + 495, $this->y, 'UTF-8');
        $page->drawText(__("FL"), $this->x + 530, $this->y, 'UTF-8');
    }

    /**
     * Draw the Data to the table in the PDF
     *
     * @param $page
     * @param $user
     * @return void
     */
    private function drawTableContent($page, $user)
    {
        $page->drawText($user->getName(), $this->x + 10, $this->y, 'UTF-8');
        $page->drawText($user->getEmail(), $this->x + 100, $this->y, 'UTF-8');
        $page->drawText($user->getStreet(), $this->x + 240, $this->y, 'UTF-8');
        $page->drawText($user->getCity() . ', ' . $user->getPostCode() . ', ' . $user->getState(), $this->x + 240,
            $this->y - 10, 'UTF-8');
        $page->drawText($user->getNationality(), $this->x + 380, $this->y, 'UTF-8');
        $page->drawText($user->getWeather(), $this->x + 450, $this->y, 'UTF-8');
        $page->drawText($user->getTemperature(), $this->x + 495, $this->y, 'UTF-8');
        $page->drawText($user->getFeelsLike(), $this->x + 530, $this->y, 'UTF-8');
    }

    /**
     * Handle redirection when Exception is raised during PDF creation
     *
     * @return ResultInterface
     */
    private function handleException()
    {
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $filters = $this->buildCollectionFilters->getFilters($this->request);
        $this->messageManager->addErrorMessage(__('Could not generate the PDF. Please try again later.'));
        $redirect->setUrl('/navipartner/index/grid' . '?' . http_build_query($filters));

        return $redirect;
    }
}
