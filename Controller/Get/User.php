<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Controller\Get;

use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json as JsonSerializer;
use Magento\Framework\View\Result\PageFactory;
use NaviPartner\BackendTest\Api\Data\UserInterface;
use NaviPartner\BackendTest\Api\Data\WeatherInterface;
use NaviPartner\BackendTest\Api\UserRepositoryInterface;
use NaviPartner\BackendTest\Api\WeatherRepositoryInterface;
use NaviPartner\BackendTest\Block\Get\User as UserBlock;
use NaviPartner\BackendTest\Logger\Logger;
use NaviPartner\BackendTest\Model\RandomUser\RandomUser;
use NaviPartner\BackendTest\Model\WeatherInformation\WeatherInformationApi;

class User implements HttpPostActionInterface
{

    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var RequestInterface
     */
    private $request;

    /**
     * @var RandomUser
     */
    private $randomUserApi;

    /**
     * @var WeatherInformationApi
     */
    private $weatherInformationApi;
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var WeatherRepositoryInterface
     */
    private $weatherRepository;

    /**
     * @var WeatherInterface
     */
    private $weather;

    /**
     * @var JsonSerializer
     */
    private $json;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @param PageFactory $resultPageFactory
     * @param JsonFactory $resultJsonFactory
     * @param RequestInterface $request
     * @param RandomUser $randomUserApi
     * @param WeatherInformationApi $weatherInformationApi
     * @param UserRepositoryInterface $userRepository
     * @param UserInterface $user
     * @param WeatherRepositoryInterface $weatherRepository
     * @param WeatherInterface $weather
     * @param JsonSerializer $json
     * @param Logger $logger
     */
    public function __construct(
        PageFactory $resultPageFactory,
        JsonFactory $resultJsonFactory,
        RequestInterface $request,
        RandomUser $randomUserApi,
        WeatherInformationApi $weatherInformationApi,
        UserRepositoryInterface $userRepository,
        UserInterface $user,
        WeatherRepositoryInterface $weatherRepository,
        WeatherInterface $weather,
        JsonSerializer $json,
        Logger $logger
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->request = $request;
        $this->randomUserApi = $randomUserApi;
        $this->weatherInformationApi = $weatherInformationApi;
        $this->userRepository = $userRepository;
        $this->user = $user;
        $this->weatherRepository = $weatherRepository;
        $this->weather = $weather;
        $this->json = $json;
        $this->logger = $logger;
    }

    /**
     * Call the User and Weather APIs and pass all the data to the block
     *
     * @return Json|false
     */
    public function execute()
    {
        $result = $this->resultJsonFactory->create();
        $resultPage = $this->resultPageFactory->create();
        $params = $this->request->getParams();
        $selectedCountry = $params['selected_country'];

        $userData = $this->randomUserApi->getUserData($selectedCountry);

        if (!$userData) {
            return false;
        }

        $coordinates = $userData['coordinates'];
        $weatherData = $this->weatherInformationApi->getWeatherInformation(
            $coordinates['latitude'],
            $coordinates['longitude']
        );

        $saveUserData = $userData;
        $user = $this->user->setData($saveUserData);
        $user->setCoordinates($this->json->serialize($userData['coordinates']));
        try {
            $user = $this->userRepository->save($user);

            $weather = $this->weather->setData($weatherData);
            $weather->setUserId($user->getEntityId());
            $this->weatherRepository->save($weather);
        } catch (LocalizedException $e) {
            $this->logger->error('Could not save the User data', [$e->getMessage()]);

            return false;
        }

        $data = [
            'user_data' => $userData,
            'weather_data' => $weatherData
        ];

        $block = $resultPage->getLayout()
            ->createBlock(UserBlock::class)
            ->setTemplate('NaviPartner_BackendTest::get/user.phtml')
            ->setData('data', $data)
            ->toHtml();

        $result->setData(['output' => $block]);

        return $result;
    }
}
