<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api;

interface GetUserInformationInterface
{
    /**
     * @return array
     */
    public function execute(): array;
}
