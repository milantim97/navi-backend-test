<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api\Data;

interface WeatherInterface
{

    const FEELS_LIKE = 'feels_like';
    const WEATHER = 'weather';
    const PRESSURE = 'pressure';
    const TEMP = 'temp';
    const HUMIDITY = 'humidity';
    const ENTITY_ID = 'entity_id';
    const USER_ID = 'user_id';
    const CHECKED_AT = 'checked_at';

    /**
     * Get entity_id
     *
     * @return string|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     *
     * @param string $entityId
     * @return WeatherInterface
     */
    public function setEntityId($entityId);

    /**
     * Get weather
     *
     * @return string|null
     */
    public function getWeather();

    /**
     * Set weather
     *
     * @param string $weather
     * @return WeatherInterface
     */
    public function setWeather($weather);

    /**
     * Get temp
     *
     * @return string|null
     */
    public function getTemp();

    /**
     * Set temp
     *
     * @param string $temp
     * @return WeatherInterface
     */
    public function setTemp($temp);

    /**
     * Get feels_like
     *
     * @return string|null
     */
    public function getFeelsLike();

    /**
     * Set feels_like
     *
     * @param string $feelsLike
     * @return WeatherInterface
     */
    public function setFeelsLike($feelsLike);

    /**
     * Get pressure
     *
     * @return string|null
     */
    public function getPressure();

    /**
     * Set pressure
     *
     * @param string $pressure
     * @return WeatherInterface
     */
    public function setPressure($pressure);

    /**
     * Get humidity
     *
     * @return string|null
     */
    public function getHumidity();

    /**
     * Set humidity
     *
     * @param string $humidity
     * @return WeatherInterface
     */
    public function setHumidity($humidity);

    /**
     * Get user_id
     *
     * @return string|null
     */
    public function getUserId();

    /**
     * Set user_id
     *
     * @param string $userId
     * @return WeatherInterface
     */
    public function setUserId($userId);

    /**
     * Get Checked At
     *
     * @return string|null
     */
    public function getCheckedAt();

    /**
     * Set Checked At
     *
     * @param string $checkedAt
     * @return WeatherInterface
     */
    public function setCheckedAt($checkedAt);
}
