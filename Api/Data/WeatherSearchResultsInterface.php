<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface WeatherSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get Weather list.
     *
     * @return \NaviPartner\BackendTest\Api\Data\WeatherInterface[]
     */
    public function getItems();

    /**
     * Set entity_id list.
     *
     * @param \NaviPartner\BackendTest\Api\Data\WeatherInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
