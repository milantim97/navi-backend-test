<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api\Data;

interface UserInterface
{

    const PHONE = 'phone';
    const EMAIL = 'email';
    const NAME = 'name';
    const STATE = 'state';
    const STREET = 'street';
    const DOB = 'dob';
    const CITY = 'city';
    const COORDINATES = 'coordinates';
    const ENTITY_ID = 'entity_id';
    const POSTCODE = 'postcode';
    const CREATED_AT = 'created_at';
    const NATIONALITY = 'nationality';

    /**
     * Get entity_id
     *
     * @return string|null
     */
    public function getEntityId();

    /**
     * Set entity_id
     *
     * @param string $entityId
     * @return UserInterface
     */
    public function setEntityId($entityId);

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Set name
     *
     * @param string $name
     * @return UserInterface
     */
    public function setName($name);

    /**
     * Get street
     *
     * @return string|null
     */
    public function getStreet();

    /**
     * Set street
     *
     * @param string $street
     * @return UserInterface
     */
    public function setStreet($street);

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity();

    /**
     * Set city
     *
     * @param string $city
     * @return UserInterface
     */
    public function setCity($city);

    /**
     * Get state
     *
     * @return string|null
     */
    public function getState();

    /**
     * Set state
     *
     * @param string $state
     * @return UserInterface
     */
    public function setState($state);

    /**
     * Get phone
     *
     * @return string|null
     */
    public function getPhone();

    /**
     * Set phone
     *
     * @param string $phone
     * @return UserInterface
     */
    public function setPhone($phone);

    /**
     * Get postcode
     *
     * @return string|null
     */
    public function getPostcode();

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return UserInterface
     */
    public function setPostcode($postcode);

    /**
     * Get email
     *
     * @return string|null
     */
    public function getEmail();

    /**
     * Set email
     *
     * @param string $email
     * @return UserInterface
     */
    public function setEmail($email);

    /**
     * Get dob
     *
     * @return string|null
     */
    public function getDob();

    /**
     * Set dob
     *
     * @param string $dob
     * @return UserInterface
     */
    public function setDob($dob);

    /**
     * Get coordinates
     *
     * @return string|null
     */
    public function getCoordinates();

    /**
     * Set coordinates
     *
     * @param string $coordinates
     * @return UserInterface
     */
    public function setCoordinates($coordinates);

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Created At
     *
     * @param string $createdAt
     * @return UserInterface
     */
    public function setCreatedAt($createdAt);

    /**
     * Get Nationality
     *
     * @return string|null
     */
    public function getNationality();

    /**
     * Set Nationality
     *
     * @param $nationality
     * @return UserInterface
     */
    public function setNationality($nationality);
}

