<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface UserSearchResultsInterface extends SearchResultsInterface
{

    /**
     * Get User list.
     *
     * @return \NaviPartner\BackendTest\Api\Data\UserInterface[]
     */
    public function getItems();

    /**
     * Set entity_id list.
     *
     * @param \NaviPartner\BackendTest\Api\Data\UserInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

