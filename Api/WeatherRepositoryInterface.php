<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use NaviPartner\BackendTest\Api\Data\WeatherInterface;
use NaviPartner\BackendTest\Api\Data\WeatherSearchResultsInterface;

interface WeatherRepositoryInterface
{

    /**
     * Save Weather
     *
     * @param WeatherInterface $weather
     * @return WeatherInterface
     * @throws LocalizedException
     */
    public function save(WeatherInterface $weather);

    /**
     * Retrieve Weather
     *
     * @param string $entityId
     * @return WeatherInterface
     * @throws LocalizedException
     */
    public function get($entityId);

    /**
     * Retrieve Weather matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return WeatherSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete Weather
     *
     * @param WeatherInterface $weather
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(WeatherInterface $weather);

    /**
     * Delete Weather by ID
     *
     * @param string $entityId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($entityId);
}

