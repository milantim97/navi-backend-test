<?php

declare(strict_types=1);

namespace NaviPartner\BackendTest\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use NaviPartner\BackendTest\Api\Data\UserInterface;
use NaviPartner\BackendTest\Api\Data\UserSearchResultsInterface;

interface UserRepositoryInterface
{

    /**
     * Save User
     *
     * @param UserInterface $user
     * @return UserInterface
     * @throws LocalizedException
     */
    public function save(UserInterface $user);

    /**
     * Retrieve User
     *
     * @param string $entityId
     * @return UserInterface
     * @throws LocalizedException
     */
    public function get($entityId);

    /**
     * Retrieve User matching the specified criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return UserSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(SearchCriteriaInterface $searchCriteria);

    /**
     * Delete User
     *
     * @param UserInterface $user
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(UserInterface $user);

    /**
     * Delete User by ID
     *
     * @param string $entityId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($entityId);
}

